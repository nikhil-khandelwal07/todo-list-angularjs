app.controller("rowCtrl", ['$scope', function ($scope) {
    $scope.list = [];
    $scope.addTask = function () {
        if (!_.isEmpty($scope.taskName)) {
            $scope.list.push({
                id: _.uniqueId("Item_"),
                taskName: $scope.taskName,
                isStriked: false,
                isDisabled: true
            });
            $scope.taskName = "";
            
        } else {
            alert("Please write something");
        }
    }
    $scope.close = function (param) {
        $scope.list = _.reject($scope.list, function (item) {
            return param.id == item.id;
        });
    }
    $scope.edit = function (param) {
        param.isDisabled = !param.isDisabled;
        param.isStriked="false";
    }
    $scope.blur = function (param) {
        param.isDisabled = !param.isDisabled;
        param.isStriked=!param.isStriked;
    }
}]);