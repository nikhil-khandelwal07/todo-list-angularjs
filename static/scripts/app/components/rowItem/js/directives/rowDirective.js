app.directive('rowDirective',
    function () {
        return{
            restrict: 'EA',
            templateUrl:Url.resolveTemplateUrl('rowItem/partials/row.html'),
            controller: 'rowCtrl'
        }
    }
);